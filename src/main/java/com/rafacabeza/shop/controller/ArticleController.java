package com.rafacabeza.shop.controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rafacabeza.shop.model.Article;

@Controller
public class ArticleController {
	@RequestMapping(value = "/articles", method = RequestMethod.GET)
	public String articles(Model model) {
		
		ArrayList<Article> articles = new ArrayList();
		articles.add(new Article(1, "ART1", "articulo 1", 35));
		articles.add(new Article(2, "ART2", "articulo 2", 35));
		articles.add(new Article(3, "ART3", "articulo 3", 35));
		articles.add(new Article(4, "ART4", "articulo 4", 35));
		
		model.addAttribute("articles", articles);

		
		return "articles";
	}	
	
	@RequestMapping(value = "/articles/{id}", method = RequestMethod.GET)
	public String article(Model model, @PathVariable("id") int id) {
		String name = "Teclado USB";
		double price = 12.5;
		String code = "KB01";
		
		model.addAttribute("id", id);
		model.addAttribute("name", name);
		model.addAttribute("code", code);
		model.addAttribute("price", price);
		
		return "article";
	}	
	


}
