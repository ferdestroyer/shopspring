<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>    
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:url value="/" var="urlPublic" />
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style>
table, th, td {
   border: 1px solid black;
}
</style>
<title>Insert title here</title>
</head>
<body>

<h1>Lista de artículos</h1>

<table>
<c:forEach var="article" items="${ articles }">
	<tr>
	<td>${ article.id }</td>
	<td>${ article.code }</td>
	<td>${ article.name }</td>
	<td>${ article.price }</td>
	<td><a href="${ urlPublic }/articles/${article.id}">Ver</a>
	<td><a href="${ urlPublic }/articles/${article.id}/edit">Editar</a>
	</tr>
</c:forEach>
</table>

<p>Página ${ page }</p>

<a href="${ urlPublic }/articles/?page=1">1</a> - 
<a href="${ urlPublic }/articles/?page=2">2</a> - 
<a href="${ urlPublic }/articles/?page=3">3</a> - 
<a href="${ urlPublic }/articles/?page=4">4</a>
</body>
</html>